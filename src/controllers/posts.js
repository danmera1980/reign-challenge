const {Post, Op} = require('../db');
const axios = require('axios');
var cron = require('node-cron');
const ip = require('ip')
const ipAddress = ip.address()

const getPostsFromApi = async (next) => {
    const apiPosts = await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
    try {
        const allData = await apiPosts.data.hits.map(p => {
            Post.findOrCreate({
                where: {
                    title: p.title==null?p.story_title:p.title,
                    author: p.author,
                    url: p.url==null?p.story_url==null?'':p.story_url:p.url,
                    tags: p._tags.toString(),
                    comment: p.comment_text==null?'':p.comment_text,
                    created: p.created_at
                }
            })
        });
    } catch (error) {
        console.log(error);
    }
}

getPostsFromApi();

cron.schedule('0 * * * *', () => {
    getPostsFromApi()
});


const getAllPosts = async (req, res, next) => {
    const {page, author, tag, title} = req.query
    const numItems = 5
    var offset = numItems * (page?page-1:0)
    let results = ''
    try {
        const allPosts = await Post.findAll({
            attributes: ['id', 'title', 'author', 'url', 'tags', 'comment', 'created'],
            where: {
                author: author?author:{[Op.like]: '%'},
                title: title?{[Op.like]: '%'+title+'%'}:{[Op.like]: '%'},
                tags: tag?{[Op.like]: '%'+tag+'%'}:{[Op.like]: '%'}
            },
            offset: offset,
            limit: numItems,
            order: [
                ['id','ASC']
            ]
        })
        const numberPosts = await Post.count();
        let nextPage = page?parseInt(page)+1:2;
        let lastPage = Math.round(numberPosts/5);
        if((page?page:1)<=lastPage){
                results = {
                posts: numberPosts,
                nextPage: page==lastPage?'This is the last page':`http://${ipAddress}:3001/post?page=${nextPage}`,
                lastPage: `http://${ipAddress}:3001/post?page=${lastPage}`,
                data: allPosts
            }
        } else results = "No data"
        res.status(200).send(results); 
    } 
    catch (e) {
        next(e)
    }
};

const deletePost = async (req, res, next) => {
    const {id} = req.query;
    try {
        id?id
        : res.status(400).send("enter an id value")
        const result = await Post.destroy({
            where: {
                id
            }
        })
    
        result?
        res.status(200).send("Post deleted"):
        res.status(400).send("Id not found or already deleted")
    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    getAllPosts,
    deletePost
}