const {Router} = require("express");

const {getAllPosts, deletePost} = require('../controllers/posts');

const router = Router();


router.get('/', getAllPosts);
router.delete('/', deletePost);

module.exports = router;