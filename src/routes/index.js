const { Router } = require('express');
const postRoute = require('./postRoute');

const router = Router();

router.use('/post', postRoute);

module.exports = router;