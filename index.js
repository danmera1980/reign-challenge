const server = require("./src/app.js");
const { conn } = require("./src/db.js");
const ip = require('ip');
const ipAddress = ip.address();

let port = 3001
// Syncing all the models at once.
conn.sync({ force: false }).then(() => {
  server.listen(port, () => {
    console.log(`API server running via http://${ipAddress}:${port}/`); // eslint-disable-line no-console
  });
});