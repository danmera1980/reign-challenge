const { expect } = require('chai');
const session = require('supertest-session');
const app = require('../../src/app.js');
const { Post, conn } = require('../../src/db.js');

const agent = session(app);

describe('Post routes', () => {
  before(() => conn.authenticate()
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  }));
  describe('GET /post', () => {
    it('should get 200', () =>
      agent.get('/post').expect(200)
    );
  });
  describe('GET /post?author',()=>{
    it('should get 200',()=>
    agent.get('/post?author=Someone').expect(200))
  });
  describe('GET /post?tag=author', ()=>{
    it('should get 200',()=>
    agent.get('/post?tag=author').expect(200))
  });
  describe('DELETE /post?id=1', ()=>{
    it('should get 200',()=>
    agent.delete('/post?id=1').expect(200))
    it('should get 400',()=>
    agent.delete('/post?id=1').expect(400))
  });
  
});