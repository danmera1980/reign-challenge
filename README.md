# Reign Challenge - Junior Back End Developer

This is my repo with the code for the Reign Challege, for a spot as a Junio Back End Developer

## Features
- The application connects to Hacker News and populates the database every hour.
- A user can recover the posted articles from the database using the REST API.
- When calling the API, the results come in 5 items and are paginated
- A user can eliminate a specific post from the database.

## Technologies

To complete this challenge the following tools were used: 
- Database: PostgreSQL
- Libraries: NodeJS, ExpressJS
- ORM: Sequelize
- Testing:  Mocha, Chai
- Test Coverage: nyc

## Setup

### Database
A postgres database needs to be created, fill the `.env` file with information related to the database and server:

    DB_USER=
    DB_PASSWORD=
    DB_HOST=
    DATABASE=

After there is a conection with the database, and the application starts, the data will be populated.

### Running the API

To set up the application, clone the repo and run the following code:
```
$ npm install
```

In order to run the API run the following code:
```
$ npm start
```

Make sure the database server is running, then you will see the address and port to call the API


> API server running via http://192.168.100.100:3001/

## Testing
To test some functions in the server run the following line: 

```
$ npm test
```

You will see the results for the test.


### Test Coverage
For running the test coverage execute the following command:
```
$ npm run coverage
```


**Note:** Stop the tests by cancelling the process, by pressing `ctrl+c`

## Postman
The file [Posts.postman_collection.json](Posts.postman_collection.json) contains calls to the API, remember to change the ip address from your server.
